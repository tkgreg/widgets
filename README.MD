#### Widgets service

What's implemented

* POST `/api/widgets/widget` - add widget

```
// Request body:
{
  "x": 100,
  "y": 100,
  "z": 0,
  "height": 100,
  "width": 100
}
```
* PUT `/api/widgets/widget` – update widget
* DELETE `/api/widgets/widget/{id}` – delete widget
* GET `/api/widgets/widget/{id}` – get widget
* GET `/api/widgets/list/{offset}/{limit}` – list widgets with pagination
* POST `/api/widgets/filter/{offset}/{limit}` – list widgets with a filter and pagination

```
// Request body:
{
  "lowX": 0,
  "lowY": 0,
  "highX": 100,
  "highY": 150
}
```

For the filtering part I used `KDTree` data structure from the http://java-ml.sourceforge.net/ library 
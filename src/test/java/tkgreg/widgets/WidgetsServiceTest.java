package tkgreg.widgets;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tkgreg.widgets.models.Rectangle;
import tkgreg.widgets.models.Widget;
import tkgreg.widgets.models.WidgetList;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class WidgetsServiceTest {

    private WidgetsService widgetsService;
    private UUID created0;
    private UUID created1;
    private UUID created2;
    private UUID created3;

    @BeforeEach
    public void init() {
        widgetsService = new WidgetsService();
        Widget widget0 = new Widget()
                .setZ(0)
                .setX(50)
                .setY(50)
                .setHeight(100)
                .setWidth(100);
        Widget widget1 = new Widget()
                .setZ(1)
                .setX(60)
                .setY(60)
                .setHeight(200)
                .setWidth(200);
        Widget widget2 = new Widget()
                .setZ(2)
                .setX(70)
                .setY(70)
                .setHeight(300)
                .setWidth(300);
        Widget widget3 = new Widget()
                .setZ(1)
                .setX(80)
                .setY(80)
                .setHeight(400)
                .setWidth(400);
        created0 = widgetsService.create(widget0).getId();
        created1 = widgetsService.create(widget1).getId();
        created2 = widgetsService.create(widget2).getId();
        created3 = widgetsService.create(widget3).getId();
    }

    private void assertWidgetZ(UUID id, int z) {
        Widget widget = widgetsService.get(id);
        assertEquals(widget.getZ(), z);
    }

    @Test
    public void testCreateWithoutZ() {
        Widget widget = new Widget()
                .setX(50)
                .setY(50)
                .setHeight(100)
                .setWidth(100);
        Widget created = widgetsService.create(widget);
        assertNotNull(created.getId());
        assertNotNull(created.getModified());
        assertEquals(widget.getZ(), 4);
    }

    @Test
    public void testCreationWithShift() {
        assertWidgetZ(created0, 0);
        assertWidgetZ(created3, 1);
        assertWidgetZ(created1, 2);
        assertWidgetZ(created2, 3);
    }

    @Test
    public void testUpdateWithShift() {
        Widget widget = new Widget()
                .setHeight(400)
                .setWidth(400)
                .setX(50)
                .setY(50)
                .setZ(2)
                .setId(created0);

        widgetsService.update(widget);

        assertWidgetZ(created3, 1);
        assertWidgetZ(created0, 2);
        assertWidgetZ(created1, 3);
        assertWidgetZ(created2, 4);
    }

    @Test
    public void testList() {
        WidgetList page0 = widgetsService.list(0, 2);
        assertEquals(4, page0.getTotal());
        assertEquals(2, page0.getWidgets().size());
        assertEquals(created0, page0.getWidgets().get(0).getId());
        assertEquals(created3, page0.getWidgets().get(1).getId());

        WidgetList page1 = widgetsService.list(2, 2);
        assertEquals(4, page1.getTotal());
        assertEquals(2, page1.getWidgets().size());
        assertEquals(created1, page1.getWidgets().get(0).getId());
        assertEquals(created2, page1.getWidgets().get(1).getId());
    }

    @Test
    public void testRemove() {
        widgetsService.delete(created3);
        Widget widget = widgetsService.get(created3);
        assertNull(widget);
    }

    @Test
    public void testFilter() {
        WidgetsService widgetsService = new WidgetsService();
        Widget widget0 = new Widget()
                .setZ(0)
                .setX(50)
                .setY(50)
                .setHeight(100)
                .setWidth(100);
        Widget widget1 = new Widget()
                .setZ(1)
                .setX(50)
                .setY(100)
                .setHeight(100)
                .setWidth(100);
        Widget widget2 = new Widget()
                .setZ(2)
                .setX(100)
                .setY(100)
                .setHeight(100)
                .setWidth(100);
        widgetsService.create(widget0);
        widgetsService.create(widget1);
        widgetsService.create(widget2);
        WidgetList result = widgetsService.filter(0, 1, new Rectangle()
                .setLowX(0)
                .setLowY(0)
                .setHighX(100).setHighY(150)
        );

        assertEquals(2, result.getTotal());
        assertEquals(0, result.getWidgets().get(0).getZ());

        WidgetList result0 = widgetsService.filter(1, 1, new Rectangle()
                .setLowX(0)
                .setLowY(0)
                .setHighX(100).setHighY(150)
        );
        assertEquals(1, result0.getWidgets().get(0).getZ());
    }

    @Test
    public void testFilterWithTreeUpdate() {
        WidgetsService widgetsService = new WidgetsService();
        Widget widget0 = new Widget()
                .setZ(0)
                .setX(50)
                .setY(50)
                .setHeight(100)
                .setWidth(100);
        Widget widget1 = new Widget()
                .setZ(1)
                .setX(50)
                .setY(100)
                .setHeight(100)
                .setWidth(100);
        Widget widget2 = new Widget()
                .setZ(2)
                .setX(100)
                .setY(100)
                .setHeight(100)
                .setWidth(100);
        Widget widget3 = new Widget()
                .setZ(1)
                .setX(50)
                .setY(100)
                .setHeight(100)
                .setWidth(100);
        widgetsService.create(widget0);
        widgetsService.create(widget1);
        widgetsService.create(widget2);
        widgetsService.create(widget3);
        WidgetList result = widgetsService.filter(0, 3, new Rectangle()
                .setLowX(0)
                .setLowY(0)
                .setHighX(100).setHighY(150)
        );

        assertEquals(3, result.getTotal());
        assertEquals(0, result.getWidgets().get(0).getZ());
        assertEquals(1, result.getWidgets().get(1).getZ());
        assertEquals(2, result.getWidgets().get(2).getZ());
    }

}
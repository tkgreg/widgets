package tkgreg.widgets;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class WidgetsControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void testWrongCreate() throws Exception {
        //language=JSON
        String widget = "{\n" +
                "  \"x\": 1,\n" +
                "  \"z\": 0,\n" +
                "  \"height\": 100,\n" +
                "  \"width\": 100\n" +
                "}";
        mockMvc.perform(post("/api/widgets/widget")
                .content(widget)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(400));
    }

    @Test
    void testCreate() throws Exception {
        //language=JSON
        String widget = "{\n" +
                "  \"x\": 1,\n" +
                "  \"y\": 0,\n" +
                "  \"z\": 0,\n" +
                "  \"height\": 100,\n" +
                "  \"width\": 100\n" +
                "}";
        mockMvc.perform(post("/api/widgets/widget")
                .content(widget)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}
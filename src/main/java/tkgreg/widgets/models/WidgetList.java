package tkgreg.widgets.models;

import java.util.List;

public class WidgetList {
    private int total;
    private List<Widget> widgets;

    public WidgetList(int total, List<Widget> widgets) {
        this.total = total;
        this.widgets = widgets;
    }

    public int getTotal() {
        return total;
    }

    public List<Widget> getWidgets() {
        return widgets;
    }
}

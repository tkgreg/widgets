package tkgreg.widgets.models;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

public class Widget {

    private UUID id;
    private Integer x;
    private Integer y;
    private Integer z;
    private Integer height;
    private Integer width;
    private LocalDateTime modified;

    public Widget(UUID id,
                  Integer x,
                  Integer y,
                  Integer z,
                  Integer height,
                  Integer width,
                  LocalDateTime modified) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.z = z;
        this.height = height;
        this.width = width;
        this.modified = modified;
    }

    public Widget() {

    }

    public UUID getId() {
        return id;
    }

    public Widget setId(UUID id) {
        this.id = id;
        return this;
    }

    public Integer getX() {
        return x;
    }

    public Widget setX(Integer x) {
        this.x = x;
        return this;
    }

    public Integer getY() {
        return y;
    }

    public Widget setY(Integer y) {
        this.y = y;
        return this;
    }

    public Integer getZ() {
        return z;
    }

    public Widget setZ(Integer z) {
        this.z = z;
        return this;
    }

    public Integer getHeight() {
        return height;
    }

    public Widget setHeight(Integer height) {
        this.height = height;
        return this;
    }

    public Integer getWidth() {
        return width;
    }

    public Widget setWidth(Integer width) {
        this.width = width;
        return this;
    }

    public LocalDateTime getModified() {
        return modified;
    }

    public Widget setModified(LocalDateTime modified) {
        this.modified = modified;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Widget widget = (Widget) o;
        return Objects.equals(id, widget.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public Widget copy() {
        return new Widget(
                this.id,
                this.x,
                this.y,
                this.z,
                this.height,
                this.width,
                this.modified);
    }
}

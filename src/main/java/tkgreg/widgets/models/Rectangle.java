package tkgreg.widgets.models;

public class Rectangle {
    private int lowX;
    private int lowY;
    private int highX;
    private int highY;

    public int getLowX() {
        return lowX;
    }

    public Rectangle setLowX(int lowX) {
        this.lowX = lowX;
        return this;
    }

    public int getLowY() {
        return lowY;
    }

    public Rectangle setLowY(int lowY) {
        this.lowY = lowY;
        return this;
    }

    public int getHighX() {
        return highX;
    }

    public Rectangle setHighX(int highX) {
        this.highX = highX;
        return this;
    }

    public int getHighY() {
        return highY;
    }

    public Rectangle setHighY(int highY) {
        this.highY = highY;
        return this;
    }

}

package tkgreg.widgets.validation;

public class ValidationError {
    private String key;
    private String message;

    private ValidationError(String key, String message) {
        this.key = key;
        this.message = message;
    }

    public static ValidationError of(String key, String message) {
        return new ValidationError(key, message);
    }

    public String getKey() {
        return key;
    }

    public ValidationError setKey(String key) {
        this.key = key;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public ValidationError setMessage(String message) {
        this.message = message;
        return this;
    }
}

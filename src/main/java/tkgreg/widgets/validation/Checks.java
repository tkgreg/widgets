package tkgreg.widgets.validation;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Checks {

    List<ValidationError> acc = new ArrayList<>();

    public Checks isNotNull(
            String key,
            Integer value
    ) {
        if (value == null) {
            acc.add(ValidationError.of(key, "[" + key + "] is not set"));
        }
        return this;
    }

    public void validate() {
        if (acc.size() > 0) {
            String reason = this.errors()
                    .stream()
                    .map(ValidationError::getMessage).collect(Collectors.joining());
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, reason);
        }
    }

    public List<ValidationError> errors() {
        return acc;
    }
}

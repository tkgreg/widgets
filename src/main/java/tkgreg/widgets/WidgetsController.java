package tkgreg.widgets;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import tkgreg.widgets.models.Rectangle;
import tkgreg.widgets.models.Widget;
import tkgreg.widgets.models.WidgetList;
import tkgreg.widgets.validation.Checks;

import javax.validation.Valid;
import java.util.UUID;

@Controller
@RequestMapping("/api/widgets")
public class WidgetsController {

    private final WidgetsService widgetsService;

    @Autowired
    public WidgetsController(WidgetsService widgetsService) {
        this.widgetsService = widgetsService;
    }

    @PostMapping("/widget")
    @ResponseBody
    public Widget create(@Valid @RequestBody Widget widget) {
        validateCreateWidget(widget);
        return widgetsService.create(widget);
    }

    @PutMapping("/widget")
    @ResponseBody
    public Widget update(@Valid @RequestBody Widget widget) {
        validateUpdateWidget(widget);
        return widgetsService.update(widget);
    }

    @DeleteMapping("/widget/{id}")
    public void delete(@PathVariable("id") UUID id) {
        widgetsService.delete(id);
    }

    @GetMapping("/widget/{id}")
    @ResponseBody
    public Widget get(@PathVariable("id") UUID id) {
        return widgetsService.get(id);
    }

    @GetMapping("/list/{offset}/{limit}")
    @ResponseBody
    public WidgetList list(
            @PathVariable("offset") long offset,
            @PathVariable("limit") long limit
    ) {
        return widgetsService.list(offset, limit);
    }

    @PostMapping("/filter/{offset}/{limit}")
    @ResponseBody
    public WidgetList list(
            @PathVariable("offset") long offset,
            @PathVariable("limit") long limit,
            @RequestBody Rectangle rectangle
    ) {
        return widgetsService.filter(offset, limit, rectangle);
    }

    private void validateCreateWidget(Widget widget) {
        Checks checks = new Checks();
        checks
                .isNotNull("x", widget.getX())
                .isNotNull("y", widget.getY())
                .isNotNull("height", widget.getHeight())
                .isNotNull("width", widget.getWidth());
        checks.validate();
    }

    private void validateUpdateWidget(Widget widget) {
        Checks checks = new Checks();
        checks
                .isNotNull("x", widget.getX())
                .isNotNull("y", widget.getY())
                .isNotNull("z", widget.getY())
                .isNotNull("height", widget.getHeight())
                .isNotNull("width", widget.getWidth());
        checks.validate();
    }

}

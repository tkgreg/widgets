package tkgreg.widgets;

import org.springframework.stereotype.Service;
import tkgreg.widgets.models.Rectangle;
import tkgreg.widgets.models.Widget;
import tkgreg.widgets.models.WidgetList;

import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
public class WidgetsService {

    public static final int MAX_SIZE = 500;

    private final TreeMap<Integer, UUID> widgetsByZ = new TreeMap<>();
    private final ConcurrentHashMap<UUID, Widget> widgetsById = new ConcurrentHashMap<>();
    private final ReentrantLock lock = new ReentrantLock();
    private final KDTreeWrapper kdTreeWrapper = new KDTreeWrapper();

    public Widget create(Widget widget) {
        return withMutex(() -> {
            widget.setId(UUID.randomUUID());
            widget.setModified(LocalDateTime.now());
            if (widget.getZ() == null) {
                widget.setZ(widgetsByZ.lastKey() + 1);
            }
            return save(widget);
        });
    }

    public Widget update(Widget widget) {
        return withMutex(() -> {
            if (widgetsById.containsKey(widget.getId())) {
                int oldZ = widgetsById.get(widget.getId()).getZ();
                widget.setModified(LocalDateTime.now());
                widgetsByZ.remove(oldZ);
                widgetsById.remove(widget.getId());
                return save(widget);
            } else {
                return create(widget);
            }
        });
    }

    public void delete(UUID id) {
        withMutex(() -> {
            Widget widget = widgetsById.get(id);
            widgetsById.remove(widget.getId());
            widgetsByZ.remove(widget.getZ());
            kdTreeWrapper.delete(widget);
            return null;
        });
    }

    public Widget get(UUID id) {
        return widgetsById.get(id);
    }

    public WidgetList list(long offset, long limit) {
        return withMutex(() -> {
            long limitParam = Math.min(limit, MAX_SIZE);
            return new WidgetList(
                    widgetsByZ.size(),
                    widgetsByZ
                            .entrySet()
                            .stream()
                            .skip(offset)
                            .limit(limitParam)
                            .map(entry -> widgetsById.get(entry.getValue()))
                            .collect(Collectors.toList())
            );
        });
    }

    public WidgetList filter(long offset, long limit, Rectangle rectangle) {
        return withMutex(() -> {
            AtomicInteger total = new AtomicInteger(0);
            long limitParam = Math.min(limit, MAX_SIZE);
            List<Widget> widgets = kdTreeWrapper.range(rectangle)
                    .filter(isInRectangle(rectangle))
                    .peek(v -> total.incrementAndGet())
                    .sorted(Comparator.comparing(Widget::getZ))
                    .skip(offset)
                    .limit(limitParam)
                    .collect(Collectors.toList());
            return new WidgetList(total.intValue(), widgets);
        });
    }

    private Predicate<Widget> isInRectangle(Rectangle rectangle) {
        return widget -> {
            int minY = widget.getY() - widget.getHeight() / 2;
            int maxY = widget.getY() + widget.getHeight() / 2;
            int minX = widget.getX() - widget.getWidth() / 2;
            int maxX = widget.getX() + widget.getWidth() / 2;
            int rs = 0;
            if (maxY <= rectangle.getHighY() && minY >= rectangle.getLowY()) {
                rs++;
            }
            if (maxX <= rectangle.getHighX() && minX >= rectangle.getLowX()) {
                rs++;
            }
            return rs == 2;
        };
    }

    private Widget save(Widget widget) {
        if (widgetsByZ.containsKey(widget.getZ())) {
            NavigableMap<Integer, UUID> subMap = widgetsByZ.subMap(
                    widget.getZ(),
                    true,
                    widgetsByZ.lastKey(),
                    true
            );

            Map<Integer, Widget> toUpdate = collectForUpdate(subMap);
            toUpdate.forEach((z, w) -> {
                widgetsByZ.remove(z);
                widgetsById.put(w.getId(), w);
                kdTreeWrapper.updateByCoords(w.getX(), w.getY(), z, w);
            });
            toUpdate.forEach((key, value) -> widgetsByZ.put(value.getZ(), value.getId()));
        }
        addWidget(widget);
        return widget;
    }

    private Map<Integer, Widget> collectForUpdate(NavigableMap<Integer, UUID> subMap) {
        Integer prevKey = null;
        Map<Integer, Widget> toUpdate = new HashMap<>();
        for (Integer key : subMap.keySet()) {
            UUID widgetId = widgetsByZ.get(key);
            Widget oldWidget = widgetsById.get(widgetId);
            Optional<Widget> changed = increaseIfNeeded(prevKey, oldWidget);
            if (changed.isPresent()) {
                toUpdate.put(key, changed.get());
                prevKey = changed.get().getZ();
            } else {
                return toUpdate;
            }
        }
        return toUpdate;
    }

    private <T> T withMutex(Callable<T> callable) {
        try {
            lock.lock();
            return callable.call();
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            lock.unlock();
        }
    }

    private void addWidget(Widget widget) {
        widgetsByZ.put(widget.getZ(), widget.getId());
        widgetsById.put(widget.getId(), widget);
        kdTreeWrapper.insert(widget);
    }

    private Optional<Widget> increaseIfNeeded(Integer prevKey, Widget widget) {
        if (prevKey == null || prevKey.equals(widget.getZ())) {
            Widget copy = widget.copy();
            copy.setZ(widget.getZ() + 1);
            return Optional.of(copy);
        }
        return Optional.empty();
    }


}

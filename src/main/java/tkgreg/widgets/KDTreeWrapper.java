package tkgreg.widgets;

import net.sf.javaml.core.kdtree.KDTree;
import tkgreg.widgets.models.Rectangle;
import tkgreg.widgets.models.Widget;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

public class KDTreeWrapper {

    private final KDTree kdTree;

    public KDTreeWrapper() {
        this.kdTree = new KDTree(2);
    }

    private Map<Integer, Widget> search(Point point) {
        Object search = kdTree.search(point.toDoubleArray());
        return (Map<Integer, Widget>) search;
    }

    public void insert(Widget widget) {
        Point point = new Point(widget.getX(), widget.getY());
        Map<Integer, Widget> bucket = search(point);
        if (bucket == null) {
            bucket = new HashMap<>();
            bucket.put(widget.getZ(), widget);
            kdTree.insert(point.toDoubleArray(), bucket);
        } else {
            bucket.put(widget.getZ(), widget);
        }
    }

    public void delete(Widget widget) {
        Point point = new Point(widget.getX(), widget.getY());
        Map<Integer, Widget> bucket = search(point);
        bucket.remove(widget.getZ());
        if (bucket.size() == 0) {
            kdTree.delete(point.toDoubleArray());
        }
    }

    public void updateByCoords(int x, int y, int z, Widget widget) {
        Point point = new Point(x, y);
        Map<Integer, Widget> bucket = search(point);
        bucket.remove(z);
        bucket.put(widget.getZ(), widget);
    }

    public Stream<Widget> range(Rectangle rectangle) {
        Object[] range = kdTree.range(
                new double[]{rectangle.getLowX(), rectangle.getLowY()},
                new double[]{rectangle.getHighX(), rectangle.getHighY()});
        return Stream.of(range).flatMap(o -> {
            Map<Integer, Widget> bucket = (Map<Integer, Widget>) o;
            return bucket.values().stream();
        });
    }

    public static class Point {
        public final int x;
        public final int y;

        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public double[] toDoubleArray() {
            return new double[]{this.x, this.y};
        }
    }

}
